package me.adrianfaisal.hsv;

/**
 * Created by apldex on 6/15/17.
 */


import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.BaseImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.conf.layers.setup.ConvolutionLayerSetup;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Random;

public class App {

    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws  Exception{
        int nRows = 30;
        int nEpoch = 10;
        int nChannels = 1;
        int nColumns = 30;
        int outputNum = 26;
        int batchSize = 11;
        int iterations = 1;
        int labelIndex = 645;
        int seed = 123;
        Random randNumGen = new Random(seed);

        logger.info("Setting up dataset");
        /* ----Setting up dataset for input---- */

        /*Setup the RecordReader as an data reader for the net

         */
        RecordReader recordReader = new CSVRecordReader(0, ",");
        recordReader.initialize(new FileSplit(new File("/Volumes/Data/Dataset/Signaturev1.csv")));

        DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, 1, labelIndex, 5);

        DataSet dataSet = iterator.next(10);
        System.out.println(dataSet.toString());

         /* Setup the network architecture
        //
         */

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .iterations(iterations) // Training iterations as above
                .weightInit(WeightInit.XAVIER)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(Updater.NESTEROVS).momentum(0.9)
                .list()
                .layer(0, new ConvolutionLayer.Builder(5, 5)
                        //nIn and nOut specify depth. nIn here is the nChannels and nOut is the number of filters to be applied
                        .nIn(nChannels)
                        .stride(1, 1)
                        .nOut(20)
                        .activation(Activation.IDENTITY)
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2,2)
                        .stride(2,2)
                        .build())
                .layer(2, new ConvolutionLayer.Builder(5, 5)
                        //Note that nIn need not be specified in later layers
                        .stride(1, 1)
                        .nOut(50)
                        .activation(Activation.IDENTITY)
                        .build())
                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2,2)
                        .stride(2,2)
                        .build())
                .layer(4, new DenseLayer.Builder().activation(Activation.RELU)
                        .nOut(500).build())
                .layer(5, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(outputNum)
                        .activation(Activation.SOFTMAX)
                        .build())
                .setInputType(InputType.convolutionalFlat(28,28,1)) //See note below
                .backprop(true).pretrain(false).build();

//        MultiLayerConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
//                .seed(seed)
//                .iterations(iterations)
//                .regularization(true).l2(0.0005)
//                .learningRate(0.01)
//                .weightInit(WeightInit.XAVIER)
//                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//                .updater(Updater.NESTEROVS).momentum(0.9)
//                .list()
//                .layer(0, new ConvolutionLayer.Builder(5, 5)
//                        //Convolution Layer #1
//                        .nIn(nChannels)
//                        .name("ConvLayer #1")
//                        .stride(1, 1)
//                        .nOut(250) //(5x5x10) -> (w x h x d)
//                        .activation("identity")
//                        .build())
//                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .name("MaxPoolLayer #1")
//                        .kernelSize(2, 2)
//                        .stride(2, 2)
//                        .build())
//                .layer(2, new ConvolutionLayer.Builder(3, 3)
//                        //Convolution Layer #2
//                        .name("ConvLayer #2")
//                        .stride(2, 2)
//                        .nOut(180) //(3x3x20) -> (w x h x d)
//                        .activation("identity")
//                        .build())
//                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .name("MaxPoolLayer #2")
//                        .kernelSize(2, 2)
//                        .stride(2, 2)
//                        .build())
//                .layer(4, new DenseLayer.Builder().activation("relu")
//                        .name("ReLU layer")
//                        .nOut(180).build())
//                .layer(5, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
//                        .name("Output layer")
//                        .nOut(outputNum)
//                        .activation("softmax")
//                        .build())
//                .setInputType(InputType.convolutionalFlat(nRows, nColumns, 1))
//                .backprop(true).pretrain(false);
//
//        MultiLayerConfiguration conf = builder.build();
//        MultiLayerNetwork model = new MultiLayerNetwork(conf);

    }
}
